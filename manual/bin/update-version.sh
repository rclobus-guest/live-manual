#!/bin/sh

set -e

PUBLICATION_DATE=$(dpkg-parsechangelog --show-field Timestamp --file ../debian/changelog)
DAY="$(LC_ALL=C date --utc -d@${PUBLICATION_DATE} +%d)"
MONTH="$(LC_ALL=C date --utc -d@${PUBLICATION_DATE} +%m)"
YEAR="$(LC_ALL=C date --utc -d@${PUBLICATION_DATE} +%Y)"

echo "Updating version information..."
sed -i  -e "s|^ :published:.*$| :published: ${YEAR}-${MONTH}-${DAY}|" \
	-e "s|(C) 2016-.*|(C) 2016-${YEAR} The Debian Live team|" \
en/live-manual.ssm

# European date format
for _LANGUAGE in ca de es fr it pl ro
do
	if [ -e po/${_LANGUAGE}/live-manual.ssm.po ]
	then
		sed -i -e "s|:published: .*.${YEAR}|:published: ${DAY}.${MONTH}.${YEAR}|" \
		po/${_LANGUAGE}/live-manual.ssm.po
	fi
done

# Brazilian date format
if [ -e po/pt_BR/live-manual.ssm.po ]
then
	sed -i -e "s|:published: .*-${YEAR}|:published: ${DAY}-${MONTH}-${YEAR}|" \
	po/pt_BR/live-manual.ssm.po
fi
